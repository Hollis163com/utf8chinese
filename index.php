<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/27 0027
 * Time: 下午 10:54
 */
include_once 'utf8_chinese.class.php';
$chinese = new utf8_chinese;
//UTF8内繁转简
$str = "    讓創意、記憶、表達力 無限延伸!思維導圖 (MindMap)被廣泛運用在記憶、學習、簡報、企業管理、自我管理等領域中，也是提升集中力、創造力、表達能力的必備工具。";

echo "原文：<br /> $str <br /> <br /> ";

$str = $chinese->big5_gb2312($str);
echo "转换为简体后：<br />$str <br /> <br />";

$str = $chinese->gb2312_big5($str);
echo "再转换为繁体后：<br />$str <br /> <br />";